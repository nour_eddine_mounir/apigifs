package com.nsmobileapp.apiGifs;

import com.nsmobileapp.apiGifs.dao.CategorieRepository;
import com.nsmobileapp.apiGifs.entities.Categorie;
import com.nsmobileapp.apiGifs.services.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;

@SpringBootApplication
public class ApiGifsApplication implements CommandLineRunner {

	@Resource
	StorageService storageService;

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(ApiGifsApplication.class, args);
		CategorieRepository categorieRepository = context.getBean(CategorieRepository.class);
		/*categorieRepository.save(new Categorie("Death Note", "#323543"));
		categorieRepository.save(new Categorie("Attack on Titan", "#764387"));
		categorieRepository.save(new Categorie("Fullmetal Alchemist: Brotherhood", "#367354"));
		categorieRepository.save(new Categorie("Naruto Shippuden", "#873256"));
		categorieRepository.save(new Categorie("Naruto", "#357443"));*/
	}

	@Override
	public void run(String... strings) throws Exception {
		//storageService.deleteAll();
		//storageService.init();
	}
}
