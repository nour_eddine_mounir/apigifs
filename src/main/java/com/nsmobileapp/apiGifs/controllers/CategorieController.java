package com.nsmobileapp.apiGifs.controllers;

import com.nsmobileapp.apiGifs.entities.Categorie;
import com.nsmobileapp.apiGifs.services.ICategorieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class CategorieController{

    private static final Logger LOGGER = LoggerFactory.getLogger(CategorieController.class);
    @Autowired
    private ICategorieService categorieService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Page<Categorie>> getCategories(@RequestParam(defaultValue = "") String mc, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        Page<Categorie> categories = categorieService.getCategories(mc, page, size);
        if(categories.getTotalPages() == 0){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

}
