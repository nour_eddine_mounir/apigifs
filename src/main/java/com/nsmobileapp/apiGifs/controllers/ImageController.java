package com.nsmobileapp.apiGifs.controllers;

import com.nsmobileapp.apiGifs.dao.ImageRepository;
import com.nsmobileapp.apiGifs.entities.Categorie;
import com.nsmobileapp.apiGifs.entities.Image;
import com.nsmobileapp.apiGifs.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class ImageController {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private StorageService storageService;

    @RequestMapping(value = "/categories/{id}/gifs",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Page<Image>> getGifs(@PathVariable("id") Long id, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        Page<Image> images = imageRepository.findByCategoriesId(id, new PageRequest(page, size));
        Resource file;
        for (Image image:images.getContent()) {
            file = storageService.loadFile(image.getName());
            String url = MvcUriComponentsBuilder
                    .fromMethodName(ImageController.class, "getFile", file.getFilename()).build().toString();
            image.setUrl(url);
        }
        if(images.getTotalPages() == 0){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(images, HttpStatus.OK);
    }

    @RequestMapping(value = "/files/{filename:.+}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

}
