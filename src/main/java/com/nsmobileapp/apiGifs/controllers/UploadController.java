package com.nsmobileapp.apiGifs.controllers;

import com.nsmobileapp.apiGifs.entities.Categorie;
import com.nsmobileapp.apiGifs.services.CategorieService;
import com.nsmobileapp.apiGifs.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UploadController {

    @Autowired
    CategorieService categorieService;

    @Autowired
    StorageService storageService;
    List<String> files = new ArrayList<String>();

    @RequestMapping(value = "/uploadImage", method = RequestMethod.GET)
    public String listUploadedFiles(Model model) {
        List<Categorie> categories = categorieService.getCategories();
        model.addAttribute("categories", categories);
        return "uploadForm";
    }

    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    public String handleFileUpload(@RequestParam("file") MultipartFile[] images, Model model, Long[] categories) {
        try {
            for (MultipartFile file:images) {
                storageService.store(file, categories);
                files.add(file.getOriginalFilename());
                System.out.println("Name: " + file.getName() + ", Original Filename: " + file.getOriginalFilename());
            }
            model.addAttribute("message", "You successfully uploaded files !");
        } catch (Exception e) {
            model.addAttribute("message", "FAIL to upload files !");
        }
        List<Categorie> categoriesAtt = categorieService.getCategories();
        model.addAttribute("categories", categoriesAtt);
        return "uploadForm";
    }

    @RequestMapping(value = "/gellallfiles", method = RequestMethod.GET)
    public String getListFiles(Model model) {
        model.addAttribute("files",
                files.stream()
                        .map(fileName -> MvcUriComponentsBuilder
                                .fromMethodName(UploadController.class, "getFile", fileName).build().toString())
                        .collect(Collectors.toList()));
        model.addAttribute("totalFiles", "TotalFiles: " + files.size());
        return "listFiles";
    }

    @RequestMapping(value = "/files/{filename:.+}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

}
