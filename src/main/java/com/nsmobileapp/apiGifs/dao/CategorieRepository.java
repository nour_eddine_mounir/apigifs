package com.nsmobileapp.apiGifs.dao;

import com.nsmobileapp.apiGifs.entities.Categorie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieRepository extends JpaRepository<Categorie, Long>{

    @Query("SELECT c FROM Categorie c WHERE c.title LIKE :x")
    Page<Categorie> getCategories(@Param("x") String mc, Pageable pageable);

}
