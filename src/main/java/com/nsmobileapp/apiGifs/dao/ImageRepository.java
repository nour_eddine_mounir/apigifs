package com.nsmobileapp.apiGifs.dao;

import com.nsmobileapp.apiGifs.entities.Image;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

    @Query("select max (i.id) from Image i")
    Long getLastImageId();

    Page<Image> findByCategoriesId(Long id, Pageable pageable);

}
