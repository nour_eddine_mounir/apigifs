package com.nsmobileapp.apiGifs.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.File;
import java.io.Serializable;
import java.util.List;

@Entity
public class Image  implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private int nbrViews;
    private String alt;
    private String url;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "categories_images", joinColumns = @JoinColumn(name = "id_image", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_categorie", referencedColumnName = "id"))
    @JsonBackReference
    private List<Categorie> categories;

    public Image() {
    }

    public Image(String name, int nbrViews, String alt, String url) {
        this.name = name;
        this.nbrViews = nbrViews;
        this.alt = alt;
        this.url = url;
    }

    public Image(int nbrViews, String name, String alt, String url, List<Categorie> categories) {
        this.name = name;
        this.nbrViews = nbrViews;
        this.alt = alt;
        this.url = url;
        this.categories = categories;
    }

    public Image(String name, int nbrViews, String alt) {
        this.name = name;
        this.nbrViews = nbrViews;
        this.alt = alt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNbrViews() {
        return nbrViews;
    }

    public void setNbrViews(int nbrViews) {
        this.nbrViews = nbrViews;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Categorie> getCategories() {
        return categories;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setCategories(List<Categorie> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nbrViews=" + nbrViews +
                ", alt='" + alt + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
