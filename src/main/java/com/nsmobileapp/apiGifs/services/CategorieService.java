package com.nsmobileapp.apiGifs.services;

import com.nsmobileapp.apiGifs.dao.CategorieRepository;
import com.nsmobileapp.apiGifs.entities.Categorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategorieService implements ICategorieService{

    @Autowired
    private CategorieRepository categorieRepository;


    @Override
    public Page<Categorie> getCategories(String mc, int page, int size) {
        return categorieRepository.getCategories("%" + mc + "%", new PageRequest(page, size));
    }

    @Override
    public List<Categorie> getCategories() {
        return categorieRepository.findAll();
    }
}
