package com.nsmobileapp.apiGifs.services;


import com.nsmobileapp.apiGifs.entities.Categorie;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ICategorieService {

    Page<Categorie> getCategories(String mc, int page, int size);
    List<Categorie> getCategories();

}
