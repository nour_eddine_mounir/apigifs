package com.nsmobileapp.apiGifs.services;

import com.nsmobileapp.apiGifs.dao.CategorieRepository;
import com.nsmobileapp.apiGifs.dao.ImageRepository;
import com.nsmobileapp.apiGifs.entities.Categorie;
import com.nsmobileapp.apiGifs.entities.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class StorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorageService.class);
    private final Path rootLocation = Paths.get("upload-dir");

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private CategorieRepository categorieRepository;

    public void store(MultipartFile file, Long[] categories){
        try {
            Long lastId = imageRepository.getLastImageId();
            if(lastId == null)
                lastId = Long.valueOf(1);
            else
                lastId += 1;
            System.out.println("Last id: " + lastId);
            Files.copy(file.getInputStream(), rootLocation.resolve("gif_"+lastId+".gif"));
            Image image = new Image("gif_"+lastId+".gif", 0, file.getOriginalFilename());
            List<Categorie> categorieList = new ArrayList<>();
            for (Long categorie:categories) {
                categorieList.add(categorieRepository.findOne(categorie));
            }
            image.setCategories(categorieList);
            imageRepository.save(image);
        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }else{
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

}
